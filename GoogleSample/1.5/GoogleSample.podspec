Pod::Spec.new do |s|
  s.name         = 'GoogleSample'
  #Step 5: Update podspec (comment/uncomment)
  #s.version      = '1.4'
  s.version      = '1.5'
  s.summary      = 'Sample Project For Google'
  s.description  = 'Sample project demonstrating issues with publishing an internal pod with a dependency on GoogleAds-IMA-iOS-SDK-For-AdMob'
  s.homepage     = 'http://google.com'
  s.license      = {
                      :type => 'PROPRIETARY',
                      :text => 'Copyright 2010-2015 The Weather Channel, All Rights Reserved'
                   }
  s.author       = {
                      'Randy Fussell' => 'randy.fussell@weather.com'
                   }
  s.source       = {
                      :git => 'git@bitbucket.org:theweathercompany/google-sample.git',
                      :tag => s.version.to_s
                   }
  s.platform            = :ios, '7.0'
  s.requires_arc        = true
  s.compiler_flags      = '-fmodules'
  s.source_files        = 'GoogleSample/Shared/**/*.{h,m}'
  s.public_header_files = 'GoogleSample/Shared/**/*.h'
  s.frameworks          = 'AVFoundation', 'CoreGraphics', 'Foundation', 'UIKit'
  s.preserve_paths      = "GoogleSample/Resources/**/*.*", 'GoogleSample.xcodeproj'
  s.resource_bundle     = { 'GoogleSample' => 'GoogleSample/Resources/**/*.*' }

  s.dependency 'Google-Mobile-Ads-SDK',           '~> 7.0'
  #Step 5: Update podspec (comment/uncoment)
  #s.dependency 'GoogleAds-IMA-iOS-SDK-For-AdMob', '~> 3.0.beta.14'
  s.dependency 'GoogleAds-IMA-iOS-SDK-For-AdMob', '3.0.beta.12'

end
